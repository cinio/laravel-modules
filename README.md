# Laravel Modules
This is a wrapper package for [laravel-modules](https://github.com/nWidart/laravel-modules). This was overridden to meet our project needs.

## Install

In your composer.json add this
```json
    "autoload": {
            "psr-4": {
                "App\\": "app/",
                "Modules\\": "Modules/"
            },
            "classmap": [
                "database/seeds",
                "database/factories"
            ]
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:cinio/laravel-modules.git"
        }
```
Then run this command
<pre>composer require cinio/laravel-modules</pre>

# Artisan Commands
Basically, all the functionalities are the same as [documented](https://nwidart.com/laravel-modules/v5/introduction) in laravel module's with the exception of the following

####  module:make
```php
Description:
  Create a new module.

Usage:
  module:make [options] [--] [<name>...]

Arguments:
  name                  The names of modules will be created.

Options:
  -p, --plain           Generate a plain module (without some resources).
      --force           Force the operation to run when the module already exists.
  -a, --api             Generate a module for apis.
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
* A new option has been added `--api` to support API. 
>  For API, if `api` set to true, the folder will be generated. `generate` is ignored.
* The folder structure now supports `admin` and `member`.

#### module:route-provider
```php
Description:
  Create a new route service provider for the specified module.

Usage:
  module:route-provider [options] [--] [<module>]

Arguments:
  module                The name of module will be used.

Options:
      --force           Force the operation to run when the file already exists.
      --api             Indicates the template api for route provider
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
* The `api` option has been added for APIs.
* The route filenames have been changed, to easily identify the routes between other modules.

#### module:make-provider
```php
Description:
  Create a new service provider class for the specified module.

Usage:
  module:make-provider [options] [--] <name> [<module>]

Arguments:
  name                       The service provider name.
  module                     The name of module will be used.

Options:
      --template[=TEMPLATE]  Indicates the template service provider (master,plain,api) [default: "master"]
  -h, --help                 Display this help message
  -q, --quiet                Do not output any message
  -V, --version              Display this application version
      --ansi                 Force ANSI output
      --no-ansi              Disable ANSI output
  -n, --no-interaction       Do not ask any interactive question
      --env[=ENV]            The environment the command should run under
  -v|vv|vvv, --verbose       Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
* A new option `template` has been added.

#### module:make-contract
```php
Description:
  Create a new contract class for the specified module.

Usage:
  module:make-contract [options] [--] <name> <module>

Arguments:
  name                     The name of the contract.
  module                   The name of module will be used.

Options:
  -e, --extends[=EXTENDS]  Extends contract to other contracts. Comma separated if multiple.
  -h, --help               Display this help message
  -q, --quiet              Do not output any message
  -V, --version            Display this application version
      --ansi               Force ANSI output
      --no-ansi            Disable ANSI output
  -n, --no-interaction     Do not ask any interactive question
      --env[=ENV]          The environment the command should run under
  -v|vv|vvv, --verbose     Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
#### module:make-filter
```php
Description:
  Create a new filter class for the specified module.

Usage:
  module:make-filter <name> <module>

Arguments:
  name                  The name of the contract.
  module                The name of module will be used.

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
``` 
#### module:make-overriding
```php
Description:
  Create an overriding class.

Usage:
  module:make-overriding <name> <module>

Arguments:
  name                  The name of the commission.
  module                The name of module will be used.

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

#### module:make-query
```php
Description:
  Create a new query class for the specified module.

Usage:
  module:make-query <name> <module>

Arguments:
  name                  The name of the contract.
  module                The name of module will be used.

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

#### module:make-repository
```php
Description:
  Create a new repository class for the specified module.

Usage:
  module:make-repository [options] [--] <name> <module>

Arguments:
  name                           The name of the contract.
  module                         The name of module will be used.

Options:
  -i, --implements[=IMPLEMENTS]  Implements contracts. Comma separated if multiple.
  -u, --use[=USE]                Use traits. Comma separated if multiple.
  -h, --help                     Display this help message
  -q, --quiet                    Do not output any message
  -V, --version                  Display this application version
      --ansi                     Force ANSI output
      --no-ansi                  Disable ANSI output
  -n, --no-interaction           Do not ask any interactive question
      --env[=ENV]                The environment the command should run under
  -v|vv|vvv, --verbose           Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
#### module:make-trait
```php
Description:
  Create a new trait class for the specified module.

Usage:
  module:make-trait <name> <module>

Arguments:
  name                  The name of the contract.
  module                The name of module will be used.

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

# Tips
* To create a file under `admin` or `member` folder, you can simply add a slash
```php
php artisan module:make-controller Admin/TestController test
```
* In the config file, you can specify placeholders in your filename.
```php
'routes/web'         => 'Routes/$LOWER_NAME$.php',
'routes/api'         => 'Routes/$LOWER_NAME$_api.php',         
```