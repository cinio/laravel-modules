<?php
		
		namespace Cinio\Module\Commands;
		
		use Illuminate\Console\Command;
		use Illuminate\Support\Facades\Artisan;
		use Illuminate\Support\Str;
		
		class MakeContractAndRepository extends Command
		{
				/**
				 * The name and signature of the console command.
				 *
				 * @var string
				 */
				protected $signature = 'module:make-contract-and-repository {name} {module?}';
				
				/**
				 * The console command description.
				 *
				 * @var string
				 */
				protected $description = 'Create a new contract class and its corresponding new repository class for the specified module.';
				
				/**
				 * Create a new command instance.
				 *
				 * @return void
				 */
				public function __construct()
				{
						parent::__construct();
				}
				
				/**
				 * Execute the console command.
				 *
				 * @return mixed
				 */
				public function handle()
				{
						$name = $this->argument('name');
						$module = $this->argument('module');
						
						$args = compact('name', 'module');
						
						/******************** Create Contract ********************/
						
						Artisan::call('module:make-contract', $args);
						$output = rtrim(Artisan::output());
						$this->info($output);
						
						if (!Str::startsWith($output,'Created')) return false;
						
						$contractPath = preg_replace('/Created.*(Modules.*).php/', '$1', $output);
						$contractPath = str_replace('/', '\\', $contractPath);
						
						/******************* Create Repository *******************/
						
						$args['--implements'] = $contractPath;
						
						Artisan::call('module:make-repository', $args);
						$output = rtrim(Artisan::output());
						$this->info($output);
						
						if (!Str::startsWith($output,'Created')) return false;
						
						$repositoryPath = preg_replace('/Created.*(Modules.*).php/', '$1', $output);
						$repositoryPath = str_replace('/', '\\', $repositoryPath);
						
						/******************* Update Config File ******************/
						
						$newBindings = "        '{$contractPath}' => '{$repositoryPath}',";
						
						if (empty($module)) {
								$module = preg_replace('/Modules\\\(.*)\\\Contracts\\\.*/', '$1', $contractPath);
						}
						
						$configPath = "Modules/{$module}/Config/config.php";
						$contents = file_get_contents($configPath);
						
						$pattern = "/'bindings'\s*=>\s*\[\]/";
						
						preg_match($pattern, $contents, $matches);
						
						if (isset($matches[0])) {
								
								$bindings = $newBindings;
								
						} else {
								
								$pattern = "/'bindings'\s*=>\s*\[\s([\s\S]*?)\s*\]/";
								
								preg_match_all($pattern, $contents, $matches);
								
								if (empty($matches[1])) {
										$configPath = base_path($configPath);
										$this->warn("Not updated: {$configPath}");
										$this->warn("Update the config file manually and then run `php artisan config:cache`.");
										return false;
								}
								
								$currentBindings = $matches[1][0];
								
								if (!Str::endsWith($currentBindings, ',')) {
										$currentBindings .= ',';
								}
								
								$bindings = "{$currentBindings}\n{$newBindings}";
								
						}
						
						$replacement = "'bindings' => [\n{$bindings}\n    ]";
						
						$contents = preg_replace($pattern, $replacement, $contents);
						
						file_put_contents($configPath, $contents);
						
						$this->info('Updated : ' . base_path($configPath));
						
						$this->call('config:cache');
						
				}
		}
