<?php

namespace Cinio\Module\Commands;

use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Support\Config\GenerateConfigReader;
use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Prophecy\Exception\Doubler\ClassNotFoundException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Str;

class MakeRepository extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument name.
     *
     * @var string
     */
    protected $argumentName = 'name';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:make-repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class for the specified module.';

    /**
     * The default namespace
     * {@inheritDoc}
     * @see \Nwidart\Modules\Commands\GeneratorCommand::getDefaultNamespace()
     */
    public function getDefaultNamespace(): string
    {
        return $this->laravel['modules']->config('paths.generator.repository.path', 'Repository');
    }

    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());

        return (new Stub('/repository.stub', [
                'NAMESPACE'          => $this->getClassNamespace($module),
                'CLASS'              => $this->getClass() . 'Repository',
                'IMPLEMENT_CONTRACT' => $this->getImplementClasses(),
                'USE_TRAITS'         => $this->getUseTraits(),
                'USE_CLASSES'        => $this->getUseClasses(),
        ]))->render();
    }

    /**
     * Get use classes
     * @return string
     */
    protected function getUseClasses()
    {
        $useStatement = '';
        if ($contracts = $this->option('implements')) {
            foreach (explode(',', $contracts) as $contract) {
                $useStatement .= "\nuse " . $contract . ';';
            }
        }

        if ($traits = $this->option('use')) {
            foreach (explode(',', $traits) as $trait) {
                $useStatement .= "\nuse " . $trait . ';';
            }
        }

        if ($useStatement) {
            $useStatement .= "\n";
        }

        return $useStatement;
    }

    /**
     * Get extend classes
     * @return string
     */
    protected function getImplementClasses()
    {
        $extendStatement = '';
        if ($contracts = $this->option('implements')) {
            foreach (explode(',', $contracts) as $contract) {
                if (!interface_exists($contract)) {
                    throw new ClassNotFoundException('Interface doesn\'t exist', $contract);
                }

                if ($extendStatement) {
                    $extendStatement .= ', ';
                }
                $classNameArray = explode('\\', $contract);
                $extendStatement .= array_pop($classNameArray);
            }
        }

        if ($extendStatement) {
            return 'implements ' . $extendStatement;
        }

        return $extendStatement;
    }

    /**
     * Get use traits
     * @return string
     */
    protected function getUseTraits()
    {
        $useStatement = '';
        if ($traits = $this->option('use')) {
            $useStatement .= "\n    use ";
            foreach (explode(',', $traits) as $index => $trait) {
                if ($index) {
                    $useStatement .= ', ';
                }
                $classNameArray = explode('\\', $trait);
                $useStatement .= array_pop($classNameArray);
            }
            $useStatement .= ';';
        }

        if ($useStatement) {
            $useStatement .= "\n";
        }

        return $useStatement;
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $contractPath = GenerateConfigReader::read('repository');

        return $path . $contractPath->getPath() . '/' . $this->getFileName() . '.php';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
                ['name', InputArgument::REQUIRED, 'The name of the contract.'],
                ['module', InputArgument::REQUIRED, 'The name of module will be used.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
                ['implements', 'i', InputOption::VALUE_OPTIONAL, 'Implements contracts. Comma separated if multiple.', null],
                ['use', 'u', InputOption::VALUE_OPTIONAL, 'Use traits. Comma separated if multiple.', null],
        ];
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return Str::studly($this->argument('name')) . 'Repository';
    }
}
