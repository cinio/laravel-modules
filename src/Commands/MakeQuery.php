<?php

namespace Cinio\Module\Commands;

use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Support\Config\GenerateConfigReader;
use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Symfony\Component\Console\Input\InputArgument;
use Str;

class MakeQuery extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument name.
     *
     * @var string
     */
    protected $argumentName = 'name';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:make-query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new query class for the specified module.';

    /**
     * The default namespace
     * {@inheritDoc}
     * @see \Nwidart\Modules\Commands\GeneratorCommand::getDefaultNamespace()
     */
    public function getDefaultNamespace(): string
    {
        return $this->laravel['modules']->config('paths.generator.queries.path', 'Queries');
    }

    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());

        return (new Stub('/query.stub', [
                'NAMESPACE' => $this->getClassNamespace($module),
                'CLASS'     => $this->getClass() . 'Query',
        ]))->render();
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $contractPath = GenerateConfigReader::read('queries');

        return $path . $contractPath->getPath() . '/' . $this->getFileName() . '.php';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
                ['name', InputArgument::REQUIRED, 'The name of the contract.'],
                ['module', InputArgument::REQUIRED, 'The name of module will be used.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return Str::studly($this->argument('name')) . 'Query';
    }
}
