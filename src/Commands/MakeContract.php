<?php

namespace Cinio\Module\Commands;

use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Support\Config\GenerateConfigReader;
use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Prophecy\Exception\Doubler\ClassNotFoundException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Str;

class MakeContract extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument name.
     *
     * @var string
     */
    protected $argumentName = 'name';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:make-contract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new contract class for the specified module.';

    /**
     * The default namespace
     * {@inheritDoc}
     * @see \Nwidart\Modules\Commands\GeneratorCommand::getDefaultNamespace()
     */
    public function getDefaultNamespace(): string
    {
        return $this->laravel['modules']->config('paths.generator.contract.path', 'Contracts');
    }

    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());

        return (new Stub('/contract.stub', [
                'NAMESPACE'      => $this->getClassNamespace($module),
                'CLASS'          => $this->getClass() . 'Contract',
                'USE_CLASSES'    => $this->getUseClasses(),
                'EXTEND_CLASSES' => $this->getExtendClasses(),
        ]))->render();
    }

    /**
     * Get use classes
     * @return string
     */
    protected function getUseClasses()
    {
        $useStatement = '';
        if ($contracts = $this->option('extends')) {
            foreach (explode(',', $contracts) as $contract) {
                $useStatement .= "\nuse " . $contract . ';';
            }
        }

        if ($useStatement) {
            $useStatement .= "\n";
        }

        return $useStatement;
    }

    /**
     * Get extend classes
     * @return string
     */
    protected function getExtendClasses()
    {
        $extendStatement = '';
        if ($contracts = $this->option('extends')) {
            foreach (explode(',', $contracts) as $contract) {
                if (!interface_exists($contract)) {
                    throw new ClassNotFoundException('Interface doesn\'t exist', $contract);
                }

                if ($extendStatement) {
                    $extendStatement .= ', ';
                }
                $classNameArray = explode('\\', $contract);
                $extendStatement .= array_pop($classNameArray);
            }
        }

        if ($extendStatement) {
            return 'extends ' . $extendStatement;
        }

        return $extendStatement;
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $contractPath = GenerateConfigReader::read('contracts');

        return $path . $contractPath->getPath() . '/' . $this->getFileName() . '.php';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
                ['name', InputArgument::REQUIRED, 'The name of the contract.'],
                ['module', InputArgument::REQUIRED, 'The name of module will be used.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['extends', 'e', InputOption::VALUE_OPTIONAL, 'Extends contract to other contracts. Comma separated if multiple.', null],
        ];
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return Str::studly($this->argument('name')) . 'Contract';
    }
}
