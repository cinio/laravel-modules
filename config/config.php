<?php

use Nwidart\Modules\Activators\FileActivator;

return [

/*
|--------------------------------------------------------------------------
| Module Namespace
|--------------------------------------------------------------------------
|
| Default module namespace.
|
*/

'namespace' => 'Modules',

/*
|--------------------------------------------------------------------------
| Module Stubs
|--------------------------------------------------------------------------
|
| Default module stubs.
|
*/

'stubs' => [
    'enabled' => true,
    'path'    => base_path() . '/vendor/cinio/laravel-modules/src/Commands/stubs',
    'files'   => [
        'routes/web'         => 'Routes/$LOWER_NAME$.php',
        'routes/api'         => 'Routes/$LOWER_NAME$_api.php',
        'views/admin/index'  => 'Resources/views/admin/index.blade.php',
        'views/member/index' => 'Resources/views/member/index.blade.php',
        'scaffold/config'    => 'Config/config.php',
        'composer'           => 'composer.json',
        'assets/js/app'      => 'Resources/assets/js/app.js',
        'assets/sass/app'    => 'Resources/assets/sass/app.scss',
        'webpack'            => 'webpack.mix.js',
        'package'            => 'package.json',
    ],
    'files-api' => [
        'routes/api'      => 'Routes/$LOWER_NAME$.php',
        'scaffold/config' => 'Config/config.php',
        'composer'        => 'composer.json',
    ],
    'replacements' => [
        'routes/web'          => ['LOWER_NAME', 'STUDLY_NAME'],
        'routes/api'          => ['LOWER_NAME'],
        'webpack'             => ['LOWER_NAME'],
        'json'                => ['LOWER_NAME', 'STUDLY_NAME', 'MODULE_NAMESPACE'],
        'views/index'         => ['LOWER_NAME'],
        'views/master'        => ['LOWER_NAME', 'STUDLY_NAME'],
        'views/admin/index'   => ['LOWER_NAME'],
        'views/admin/master'  => ['LOWER_NAME', 'STUDLY_NAME'],
        'views/member/index'  => ['LOWER_NAME'],
        'views/member/master' => ['LOWER_NAME', 'STUDLY_NAME'],
        'scaffold/config'     => ['STUDLY_NAME'],
        'composer'            => [
            'LOWER_NAME',
            'STUDLY_NAME',
            'VENDOR',
            'AUTHOR_NAME',
            'AUTHOR_EMAIL',
            'MODULE_NAMESPACE',
        ],
    ],
    'gitkeep' => true,
],
'paths' => [
    /*
    |--------------------------------------------------------------------------
    | Modules path
    |--------------------------------------------------------------------------
    |
    | This path used for save the generated module. This path also will be added
    | automatically to list of scanned folders.
    |
    */

    'modules' => base_path('Modules'),
    /*
    |--------------------------------------------------------------------------
    | Modules assets path
    |--------------------------------------------------------------------------
    |
    | Here you may update the modules assets path.
    |
    */

    'assets' => public_path('modules'),
    /*
    |--------------------------------------------------------------------------
    | The migrations path
    |--------------------------------------------------------------------------
    |
    | Where you run 'module:publish-migration' command, where do you publish the
    | the migration files?
    |
    */

    'migration' => base_path('database/migrations'),
    /*
    |--------------------------------------------------------------------------
    | Generator path
    |--------------------------------------------------------------------------
    | Customise the paths where the folders will be generated.
    | Set the generate key to false to not generate that folder
    */
    'generator' => [
        'config'            => ['path' => 'Config', 'generate' => true, 'api' => true],
        'command'           => ['path' => 'Console', 'generate' => true, 'api' => true],
        'migration'         => ['path' => 'Database/Migrations', 'generate' => true, 'api' => true],
        'seeder'            => ['path' => 'Database/Seeders', 'generate' => true, 'api' => true],
        'factory'           => ['path' => 'Database/Factories', 'generate' => true, 'api' => true],
        'model'             => ['path' => 'Models', 'generate' => true, 'api' => true],
        'controller'        => ['path' => 'Http/Controllers', 'generate' => true, 'api' => true],
        'controller-admin'  => ['path' => 'Http/Controllers/Admin', 'generate' => true, 'api' => false],
        'controller-member' => ['path' => 'Http/Controllers/Member', 'generate' => true, 'api' => false],
        'middleware'        => ['path' => 'Http/Middleware', 'generate' => true, 'api' => true],
        'request'           => ['path' => 'Http/Requests', 'generate' => true, 'api' => true],
        'request-admin'     => ['path' => 'Http/Requests/Admin', 'generate' => true, 'api' => false],
        'request-member'    => ['path' => 'Http/Requests/Member', 'generate' => true, 'api' => false],
        'provider'          => ['path' => 'Providers', 'generate' => true, 'api' => true],
        'assets'            => ['path' => 'Resources/assets', 'generate' => true, 'api' => false],
        'lang'              => ['path' => 'Resources/lang', 'generate' => true, 'api' => true],
        'views'             => ['path' => 'Resources/views', 'generate' => true, 'api' => false],
        'views-admin'       => ['path' => 'Resources/views/admin', 'generate' => true, 'api' => false],
        'views-member'      => ['path' => 'Resources/views/member', 'generate' => true, 'api' => false],
        'test'              => ['path' => 'Tests', 'generate' => true, 'api' => true],
        'repository'        => ['path' => 'Repositories', 'generate' => true, 'api' => true],
        'event'             => ['path' => 'Events', 'generate' => false, 'api' => false],
        'listener'          => ['path' => 'Listeners', 'generate' => false, 'api' => false],
        'policies'          => ['path' => 'Policies', 'generate' => false, 'api' => false],
        'rules'             => ['path' => 'Http/Rules', 'generate' => true, 'api' => true],
        'jobs'              => ['path' => 'Jobs', 'generate' => false, 'api' => false],
        'emails'            => ['path' => 'Emails', 'generate' => false, 'api' => false],
        'notifications'     => ['path' => 'Notifications', 'generate' => false, 'api' => false],
        'resource'          => ['path' => 'Transformers', 'generate' => false, 'api' => true],
        'contracts'         => ['path' => 'Contracts', 'generate' => true, 'api' => true],
        'commissions'       => ['path' => 'Commissions', 'generate' => false, 'api' => false],
        'queries'           => ['path' => 'Queries', 'generate' => true, 'api' => true],
        'queries-admin'     => ['path' => 'Queries/Admin', 'generate' => true, 'api' => false],
        'queries-member'    => ['path' => 'Queries/Member', 'generate' => true, 'api' => false],
        'filters'           => ['path' => 'Queries/Filters', 'generate' => true, 'api' => true],
        'traits'            => ['path' => 'Repositories/Concerns', 'generate' => true, 'api' => true],
    ],
],
/*
|--------------------------------------------------------------------------
| Scan Path
|--------------------------------------------------------------------------
|
| Here you define which folder will be scanned. By default will scan vendor
| directory. This is useful if you host the package in packagist website.
|
*/

'scan' => [
    'enabled' => false,
    'paths'   => [
        base_path('vendor/*/*'),
    ],
],
/*
|--------------------------------------------------------------------------
| Composer File Template
|--------------------------------------------------------------------------
|
| Here is the config for composer.json file, generated by this package
|
*/

'composer' => [
    'vendor' => 'modules',
    'author' => [
        'name'  => 'Cinio',
        'email' => 'pat.abastas@gmail.com',
    ],
],
/*
|--------------------------------------------------------------------------
| Caching
|--------------------------------------------------------------------------
|
| Here is the config for setting up caching feature.
|
*/
'cache' => [
    'enabled'  => false,
    'key'      => 'laravel-modules',
    'lifetime' => 60,
],
/*
|--------------------------------------------------------------------------
| Choose what laravel-modules will register as custom namespaces.
| Setting one to false will require you to register that part
| in your own Service Provider class.
|--------------------------------------------------------------------------
*/
'register' => [
    'translations' => true,
    /**
     * load files on boot or register method
     *
     * Note: boot not compatible with asgardcms
     *
     * @example boot|register
     */
    'files' => 'register',
],
    
    /*
    |--------------------------------------------------------------------------
    | Activators
    |--------------------------------------------------------------------------
    |
    | You can define new types of activators here, file, database etc. The only
    | required parameter is 'class'.
    | The file activator will store the activation status in storage/installed_modules
    */
'activators' => [
  'file' => [
    'class' => FileActivator::class,
    'statuses-file' => base_path('modules_statuses.json'),
    'cache-key' => 'activator.installed',
    'cache-lifetime' => 604800,
  ],
],

'activator' => 'file',
];
